/*
 * Copyright (C) Secmation, LLC - All Rights Reserved
 * Unauthorized copying and use of this file, via any medium is strictly prohibited
 * Secmation, LLC Proprietary
 */

#include "rc_joystick_linux.hpp"

RCJoystick::RCJoystick(ros::NodeHandle *nh_)
{
  // XBOX Controller
  xbox_mapping.device = "/dev/input/js0";  //js2
  xbox_mapping.pitch_key = 2;
  xbox_mapping.roll_key = 3;
  xbox_mapping.throttle_key = 1; // w/s: throttle
  xbox_mapping.yaw_key = 0;
  xbox_mapping.rc_overide_key = 8;
  xbox_mapping.arm_key = 7;

  // RC Topic
  rc_topic = "RC";

  init_joy_message();
  fd = open(xbox_mapping.device.c_str(), O_NONBLOCK);

  if (fd)
  {
    ROS_INFO("Initialized XBOX Controller");
    ROS_INFO("Publisher: %s", rc_topic.c_str());
    ROS_INFO("Device Port: %s", xbox_mapping.device.c_str());
    ROS_INFO("Pitch Key: %d", xbox_mapping.pitch_key);
    ROS_INFO("Roll Key: %d", xbox_mapping.roll_key);
    ROS_INFO("Throttle Key: %d", xbox_mapping.throttle_key);
    ROS_INFO("Yaw Key: %d", xbox_mapping.yaw_key);
  }

  rc_pub_ = nh_->advertise<rosflight_msgs::RCRaw>(rc_topic.c_str(), 10);
  status_sub_ = nh_->subscribe("/status", 5, &RCJoystick::status_callback, this);
  version_sub_ = nh_->subscribe("/version", 5, &RCJoystick::version_callback, this);

  rc_timer_ = nh_->createTimer(ros::Duration(0.02), std::bind(&RCJoystick::timer_callback, this)); // RC_FREQ = 50 Hz
}

void RCJoystick::status_callback(const rosflight_msgs::Status &msg)
{
}

void RCJoystick::version_callback(const std_msgs::String &msg)
{
}

int RCJoystick::joystick_remap(int xbox_value, bool reverse = false)
{
  float m = reverse ? -0.015259 : 0.015259;
  float b = 1500;
  float y = (m * xbox_value) + b;

  return (int)(round(y));
}

void RCJoystick::init_joy_message()
{
  msg.header.stamp = ros::Time::now();
  msg.values[0] = JS_DEFAULT_VALUE; // left/right: roll
  msg.values[1] = JS_DEFAULT_VALUE; // up/down: pitch
  msg.values[2] = JS_DEFAULT_VALUE; // w/s: throttle
  msg.values[3] = JS_DEFAULT_VALUE; // a/d: yaw
  msg.values[4] = 1000;               // aux1 o: rc override toggle
  msg.values[5] = 1000;               // aux2 m: arm toggle (assumes ARM_CHANNEL=4)
  msg.values[6] = 1000;               // aux3 Unassigned
  msg.values[7] = 1000;               // aux4 Unassigned
}

void RCJoystick::timer_callback()
{
  read(fd, &e, sizeof(e));

  if (e.type == JS_EVENT_BUTTON || e.type == JS_EVENT_AXIS)
  {
    if (e.number == xbox_mapping.throttle_key)
    {
      msg.values[2] = joystick_remap(e.value, true);  // channel 3
    }
    if (e.number == xbox_mapping.roll_key)
    {
      msg.values[0] = joystick_remap(e.value, true); // channel 1
    }
    if (e.number == xbox_mapping.pitch_key)
    {
      msg.values[1] = joystick_remap(e.value); // channel 2
    }
    if (e.number == xbox_mapping.yaw_key) 
    {
      msg.values[3] = joystick_remap(e.value);  // channel 4
    }
    if (e.number == xbox_mapping.rc_overide_key)
    {
      msg.values[4] = aux1_btn.set_value(e.value) ? 2000 : 1000; // channel 5
    }
    if (e.number == xbox_mapping.arm_key)
    {
      msg.values[5] = aux2_btn.set_value(e.value) ? 2000 : 1000; // channel 6
    }
  }

  /*ROS_INFO("Ch_1: %d, Ch_2: %d, Ch_3: %d, Ch_4: %d, Aux1: %i, Aux2: %i, Aux3: %i, Aux4: %i", msg.values[0], msg.values[1], msg.values[2],
           msg.values[3], msg.values[4], msg.values[5], msg.values[6], msg.values[7]);*/

  rc_pub_.publish(msg);
}

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "rc_joystick");
  ros::NodeHandle nh;
  RCJoystick rc_joystick(&nh);
  ros::spin();
  return 0;
}
