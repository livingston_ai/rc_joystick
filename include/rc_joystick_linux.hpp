/*
 * Copyright (C) Secmation, LLC - All Rights Reserved
 * Unauthorized copying and use of this file, via any medium is strictly prohibited
 * Secmation, LLC Proprietary
 */

/**
IMPORTANT
install xboxdrv
run sudo xboxdrv -s when using controller
**/

#pragma once

#include <fcntl.h>
#include <ros/ros.h>
#include "std_msgs/String.h"
#include "rosflight_msgs/RCRaw.h"
#include "rosflight_msgs/Status.h"
#include "rc_button_pressed.hpp"

#define JS_EVENT_BUTTON 0x01 /* button pressed/released */
#define JS_EVENT_AXIS 0x02   /* joystick moved */
#define JS_EVENT_INIT 0x80   /* initial state of device */
#define JS_DEFAULT_VALUE 1500

struct js_event
{
  unsigned int time;    /* event timestamp in milliseconds */
  short value;          /* value */
  unsigned char type;   /* event type */
  unsigned char number; /* axis/button number */
};

struct js_mapping
{
  std::string device;
  unsigned int pitch_key;
  unsigned int roll_key;
  unsigned int throttle_key;
  unsigned int yaw_key;
  unsigned int rc_overide_key;
  unsigned int arm_key;
  bool is_armed;
  bool is_overided;
};

class RCJoystick
{
public:
  RCJoystick(ros::NodeHandle *nh_);

private:
  // Callbacks
  void timer_callback();
  void status_callback(const rosflight_msgs::Status &msg);
  void version_callback(const std_msgs::String &msg);
  int joystick_remap(int xbox_value, bool reverse);
  void init_joy_message();

  ros::Timer rc_timer_; // rc timer
  rosflight_msgs::RCRaw msg;

  int fd;
  struct js_event e;
  struct js_mapping xbox_mapping;
  std::string rc_topic;

  ros::Publisher rc_pub_;
  ros::Subscriber status_sub_;
  ros::Subscriber version_sub_;

  AuxButton aux1_btn;
  AuxButton aux2_btn;

};
