/*
 * Copyright (C) Secmation, LLC - All Rights Reserved
 * Unauthorized copying and use of this file, via any medium is strictly prohibited
 * Secmation, LLC Proprietary
 */

#pragma once

class AuxButton
{
public:
  AuxButton()
  {
    state = 0;
    prev_value = 0;
    curr_value = 0;
    is_active = false;
    // printf("prev: %d, curr: %d, state: %d\n", prev_value, curr_value, state);
  }

  bool set_value(int value)
  {
    // bool retval = false;
    prev_value = curr_value;
    curr_value = value;

    switch (state)
    {
    case 0:
      if (prev_value != curr_value)
        state = 1;
      break;

    case 1:
      if (prev_value != curr_value)
      {
        // retval = true;
        is_active = !is_active;
        state = 0;
      }
    }

    // printf("prev: %d, curr: %d, state: %d, retval: %d", prev_value, curr_value, state, retval);
    return is_active; // retval;
  }

private:
  unsigned int state;
  int prev_value;
  int curr_value;
  bool is_active;
};